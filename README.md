# Network Project

This is a Network Synchronization Project made as the final assignment for the Network Programming Course of a Game Dev Course. It is based on **Unity** and uses **Photon PUN 2** to handle the networking functions.

## Description

The project consists in the synchronization of a scene containing many GameObjects interacting via the *Physics Engine*. The Player controls a big cube which can interact with numerous scattered little cubes (default: 900) on the floor. 

The big cube controlled by the player can fly, repelling nearby objects, attract them, or move normally by rotating on the ground. When a little cube touches the big cube, the little cube will change colour to show it.

## Techniques

The techniques used for synchronizing the scenes is **snapshot interpolation with buffering**. Furthermore, the system only sends info about the object that are active to optimize the bandwidth used.

For the same purpose **Snapshot Compression** was used. Currently we only send the *position*, *rotation* and an *id*. The spacial information are also **quantized** and **bounded** as follows:

* **x and z coordinates**: [-128, 128]; stored as a `short`;
* **y coordinate**: [0, 16]; stored as a `byte`;
* **x, y, z quaternion coefficients**: [0, 255]; stored as a `byte`;
* **w quaternion coefficient**: calculated and not sent;
* **id**: stored as a `short`

this amounts to a packet size of only 10 bytes.

With the default settings, we have a send rate of 60 times per second. Given the default number of cubes to synchronize of 901, and that we only send active cubes, we observe an average bandwith usage of about **350 kbit/s**

To improve the visual effect of the synchronization, received **Snapshots are buffered** and the client is updated smoothly by Interpolating the current state with the received values. This makes the scene look more consistent at the cost of a small extra delay.

## Class Organization
The project is composed of just a few classes: 
* **FollowCamera**: A simple component to link camera and Player Cube;
* **Launcher**: A class used to initialize some network parameters, allow connection, and hold info during the demo. You can use it to customize **send rate**, **number of max players allowed in a room**, **Buffer size** and the **SRTT Alpha**.
* **LittleCube**: Handles the update of the state for the single cube, and the color change behaviour on collision. For the most part it's just a client of the *LittleCubesManager* class;
* **LittleCubesManager**: Encapsules the spawning and synchronization of all the little cubes in the scene. As well as showing debug informations, and passing the snapshot info to the right cube. You can use it to change the number of cubes to spawn and synchronize;
* **PlayerController**: Encapsules Player movement control and synchronization;
* **Packet**: A struct to **encapsule the state to send, compression, quantization, and network calls**. It uses mostly static calls to allow this services. It is registered to Photon to allow straigthforward use.

## The Demo
To start the demo clone and keep two copies of the project to open them via the Unity Hub. Open and start the **Launcher Scene**.

### Controls

* **WASD**: Move;
* **Left Mouse**: Fly;
* **Right Mouse**: Attract cubes;