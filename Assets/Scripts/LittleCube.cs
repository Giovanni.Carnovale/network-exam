using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody))]
public class LittleCube : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] private Color BaseColor= Color.white;
    [SerializeField, Range(0.0f, 2.0f)] private float TimeToChange = 2.0f;
    [SerializeField] private Color InteractedColor = Color.red;
    [SerializeField, Range(0.0f, 2.0f)] private float TimeBeforeGoingBack = 2.0f;

    private Renderer RendererComponent;
    private Rigidbody rb;

    private float ElapsedTime = 0.0f;
    private bool Interacted = false;

    private List<Snapshot> SnapshotBuffer = new List<Snapshot> ();

    private short id;

    public short Id
    {
        get { return id; }
        set { id = value; }
    }

    private void Awake()
    {
        RendererComponent = GetComponent<Renderer>();
        Assert.IsNotNull(RendererComponent);

        rb = GetComponent<Rigidbody>();
        Assert.IsNotNull(rb);
        rb.isKinematic = !PhotonNetwork.IsMasterClient;
    }

    void Start()
    {
        
    }

    public void FeedSnapshot(Vector3 position, Quaternion rotation)
    {
        Snapshot s = new Snapshot() { TargetPosition = position, TargetRotation = rotation };
        SnapshotBuffer.Add(s);
    }

    void Update()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            float NormalizedPacketTime = LittleCubesManager.TimeSinceLastPacket / LittleCubesManager.EstimatedTimeBetweenPackets;

            if (SnapshotBuffer.Count > 0)
            {
                Snapshot s = SnapshotBuffer[0];

                if (SnapshotBuffer.Count > Launcher.Instance.BufferSize)
                {
                    rb.MovePosition(s.TargetPosition);
                    rb.MoveRotation(s.TargetRotation);
                    SnapshotBuffer.RemoveAt(0);
                }
                else
                {
                    rb.MovePosition(Vector3.LerpUnclamped(transform.position, s.TargetPosition, NormalizedPacketTime));
                    rb.MoveRotation(Quaternion.SlerpUnclamped(transform.rotation, s.TargetRotation, NormalizedPacketTime));
                }
                if (transform.position.AlmostEquals(s.TargetPosition, 0.1f) && transform.rotation.AlmostEquals(s.TargetRotation, 1))
                {
                    SnapshotBuffer.RemoveAt(0);
                }
            }
        } else
        {
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, -128f, 128f),
                Mathf.Clamp(transform.position.y, 0f, 16f),
                Mathf.Clamp(transform.position.z, -128f, 128f)
            );
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player") && !Interacted)
        {
            Interacted = true;
            StopAllCoroutines();
            StartCoroutine(ChangeColor(InteractedColor));
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && Interacted)
        {
            Interacted = false;
            StartCoroutine(ChangeColor(BaseColor, TimeBeforeGoingBack));
        }
    }

    private IEnumerator ChangeColor(Color c, float WaitBeginning = 0.0f)
    {
        if(WaitBeginning > 0.0f)
        {
            yield return new WaitForSeconds(WaitBeginning);
            if (Interacted) yield break;
        }

        ElapsedTime = 0.0f;
        Color StartingColor = RendererComponent.material.color;
        while(ElapsedTime <= TimeToChange)
        {
            RendererComponent.material.color = Color.Lerp(StartingColor, c, ElapsedTime/TimeToChange);
            ElapsedTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    public bool IsActive()
    {
        return !rb.IsSleeping() || Interacted; 
    }
}
