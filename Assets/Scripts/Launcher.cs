using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;


public class Launcher : MonoBehaviourPunCallbacks
{
    string gameVersion = "1";
    [Header("Network Info")]
    [SerializeField, Min(0.0f)] private int sendRate = 60;
    [SerializeField, Min(0.0f)] private byte maxPlayers = 4;
    [SerializeField, Min(0.0f)] private byte bufferSize = 4;
    [SerializeField, Range(0.0f, 1.0f)] private float SRTTAlpha = 0.2f;
    [SerializeField] private Object sceneAsset = null;

    public static Launcher Instance;

    public int SendRate { get { return sendRate; } }
    public int BufferSize { get { return bufferSize; } }
    public float Alpha { get { return SRTTAlpha; } }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(this);
        }
        DontDestroyOnLoad(this);

        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.SendRate = sendRate;

        PhotonPeer.RegisterType(typeof(Packet), 0xFF, Packet.Serialize, Packet.Deserialize);
    }

    private void Start()
    {
        Connect();
    }

    public void Connect()
    {
        if(PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        } else
        {
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameVersion;
        }
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Connect();
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("OnDisconnected() was called by PUN with reason {0}", cause);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarningFormat("OnJoinRandomFailed() was called by PUN with reason {0}: {1}",returnCode, message);
        Debug.Log("Creating room");

        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayers });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom() called by PUN");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneAsset.name);
        }
    }
}
