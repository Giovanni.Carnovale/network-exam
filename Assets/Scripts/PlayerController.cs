using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
{
    [Header("References")]
    [SerializeField]
    private Rigidbody rb;

    [Header("Parameters")]
    [SerializeField, Range(0, 80f)] private float Speed = 10f;
    [SerializeField, Range(0, 50f)] private float MaxSpeed = 20f;
    [SerializeField, Range(0, 100f)] private float RepulsionForce = 10f;
    [SerializeField, Range(0, 20f)] private float RepulsionRange = 4f; 
    [SerializeField, Range(0, 100f)] private float AttractionForce = 10f;
    [SerializeField, Range(0, 20f)] private float AttractionRange = 4f;

    private List<Snapshot> SnapshotBuffer = new List<Snapshot>();


    private Vector3 forceVector = Vector3.zero;
    bool RequestedFly = false, RequestedKatamari = false;

    private float TimeSinceLastPacket = 0.0f;
    private float EstimatedTimeBetweenPackets = 1.0f/PhotonNetwork.SendRate;

    private void Awake()
    {
        Assert.IsNotNull(rb, "No Rigidbody found in PlayerController");
        rb.isKinematic = !photonView.IsMine;
    }
    void Start()
    {
        
    }
    void Update()
    {
        if(photonView && photonView.IsMine)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            RequestedFly = Input.GetMouseButton(0) && !Input.GetMouseButton(1);
            RequestedKatamari = Input.GetMouseButton(1) && !Input.GetMouseButton(0);

            forceVector = Speed * new Vector3(x, 0.0f, z);

            rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxSpeed);

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, -128f, 128f),
                Mathf.Clamp(transform.position.y, 0f, 16f),
                Mathf.Clamp(transform.position.z, -128f, 128f)
            );
        } 
        else
        {
            TimeSinceLastPacket += Time.deltaTime;
            
            if(SnapshotBuffer.Count > 0)
            {
                Snapshot s = SnapshotBuffer[0];

                if (SnapshotBuffer.Count > Launcher.Instance.BufferSize)
                {
                    rb.MovePosition(s.TargetPosition);
                    rb.MoveRotation(s.TargetRotation);
                    SnapshotBuffer.RemoveAt(0);
                } else
                {
                    rb.MovePosition(Vector3.LerpUnclamped(transform.position, s.TargetPosition, TimeSinceLastPacket / EstimatedTimeBetweenPackets));
                    rb.MoveRotation(Quaternion.SlerpUnclamped(transform.rotation, s.TargetRotation, TimeSinceLastPacket / EstimatedTimeBetweenPackets));
                }
                if(transform.position.AlmostEquals(s.TargetPosition, 0.1f) && transform.rotation.AlmostEquals(s.TargetRotation, 1))
                {
                    SnapshotBuffer.RemoveAt(0);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if(photonView && photonView.IsMine)
        {
            rb.AddForce(forceVector, ForceMode.Acceleration);

            if (RequestedFly)
            {
                Vector3 ForcePos = transform.position;
                ForcePos.y = 0;

                Collider[] CollidersInArea = Physics.OverlapSphere(ForcePos, RepulsionRange);
                foreach (Collider col in CollidersInArea)
                {
                    Rigidbody rb = col.attachedRigidbody;
                    if (rb)
                        rb.AddExplosionForce(RepulsionForce, ForcePos, RepulsionRange, 1, ForceMode.Acceleration);
                }
            }
            else if (RequestedKatamari)
            {
                Vector3 ForcePos = transform.position;

                Collider[] CollidersInArea = Physics.OverlapSphere(ForcePos, AttractionRange);
                foreach (Collider col in CollidersInArea)
                {
                    if (col.gameObject == this.gameObject) continue;

                    Rigidbody rb = col.attachedRigidbody;
                    if (rb)
                        rb.AddExplosionForce(-AttractionForce, ForcePos, AttractionRange, 1, ForceMode.Acceleration);
                }
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting && PhotonNetwork.IsMasterClient)
        {
            Packet.SendData(stream, -1, transform.position, transform.rotation);
        } 
        else if(stream.IsReading && !PhotonNetwork.IsMasterClient)
        {
            Vector3 NetPos = Vector3.zero;
            Quaternion NetRot = Quaternion.identity;
            short id;
            Packet.ReceiveData(stream, out id, out NetPos, out NetRot);
            Assert.AreEqual<short>(id, -1, "Received wrong packet for Player Controlled Cube");

            Snapshot s = new Snapshot();
            s.TargetPosition = NetPos;
            s.TargetRotation = NetRot;

            SnapshotBuffer.Add(s);

            EstimatedTimeBetweenPackets = (1f - Launcher.Instance.Alpha) * EstimatedTimeBetweenPackets + (Launcher.Instance.Alpha) * TimeSinceLastPacket;
            TimeSinceLastPacket = 0.0f;
        }
    }
}
