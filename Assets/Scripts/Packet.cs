using System;
using UnityEngine;

using Photon.Pun;
using System.Collections.Generic;

[Serializable]
struct Packet
{
    public short id;

    //x,z � [-128, 128], y � [0, 16]
    public short posX;
    public byte posY;
    public short posZ;

    //x,y,z � [0, 255]
    public byte rotX;
    public byte rotY;
    public byte rotZ;

    public override string ToString()
    {
        string str = "Id " + id + ";\nPos: ("+ posX +", " + posY +", " + posZ +"); ";
        str += "Rot: ("+ rotX +", " + rotY + ", " + rotZ + ");";
        return str;
    }

    public static int GetSize()
    {
        return 3 * sizeof(short) + 4 * sizeof(byte);
    }

    public static Packet MakePacket(short id, ref Vector3 position, ref Quaternion rotation)
    {
        Packet p = new Packet();

        p.id = id;

        p.posX = (short)Mathf.Round(position.x / 128f * short.MaxValue);
        p.posY = (byte)Mathf.Round(position.y / 16f * byte.MaxValue);
        p.posZ = (short)Mathf.Round(position.z / 128f * short.MaxValue);

        float sign = rotation.w >= 0 ? 1 : -1;
        p.rotX = (byte)(sign * rotation.x * 128f + 128);
        p.rotY = (byte)(sign * rotation.y * 128f + 128);
        p.rotZ = (byte)(sign * rotation.z * 128f + 128);

        return p;
    }

    public static Packet MakePacket(short id, Transform transform)
    {
        Vector3 position = transform.position;
        Quaternion rotation = transform.rotation;
        return MakePacket(id, ref position, ref rotation);
    }

    public static void UnpackPacket(Packet p, out short id, out Vector3 position, out Quaternion rotation)
    {
        id = p.id;

        position.x = (float)((short)p.posX * 128f / short.MaxValue);
        position.y = (float)((byte)p.posY * 16f / byte.MaxValue);
        position.z = (float)((short)p.posZ * 128f / short.MaxValue);


        rotation.x = (-128f + p.rotX) / 128f;
        rotation.y = (-128f + p.rotY) / 128f;
        rotation.z = (-128f + p.rotZ) / 128f;


        float sqrdW = 1 - (rotation.x * rotation.x + rotation.y * rotation.y + rotation.z * rotation.z);

        if (sqrdW >= 0)
        {
            rotation.w = Mathf.Sqrt(sqrdW);
        }
        else
        {
            rotation.w = 0;
        }
        rotation.Normalize();
    }

    public static void SendData(PhotonStream stream, short id, Vector3 position, Quaternion rotation)
    {
        Packet p = MakePacket(id, ref position, ref rotation);
        stream.SendNext(p);
    }

    public static void SendBuffered(PhotonStream stream, ref List<Packet> ToSend)
    {
        stream.SendNext(ToSend.Count);
        foreach(Packet p in ToSend)
        {
            stream.SendNext(p);
        }
    }

    public static void ReceiveData(PhotonStream stream, out short id, out Vector3 position, out Quaternion rotation)
    {
        Packet p = (Packet)stream.ReceiveNext();

        UnpackPacket(p, out id, out position, out rotation);

    }

    public static List<Packet> ReceiveBuffered(PhotonStream stream)
    {
        int PacketListSize = (int) stream.ReceiveNext();

        List<Packet> packets = new List<Packet>(PacketListSize);
        for(int i = 0; i<PacketListSize; i++)
        {
            Packet p = (Packet) stream.ReceiveNext();
            packets.Add(p);
        }

        return packets;
    }




    public static byte[] Serialize(object customType)
    {
        Packet c = (Packet ) customType;
        byte[] array = new byte[GetSize()];

        Array.Copy(BitConverter.GetBytes(c.id), 0, array, 0, 2);
        Array.Copy(BitConverter.GetBytes(c.posX), 0, array, 2, 2);
        Array.Copy(BitConverter.GetBytes(c.posY), 0, array, 4, 1);
        Array.Copy(BitConverter.GetBytes(c.posZ), 0, array, 5, 2);
        Array.Copy(BitConverter.GetBytes(c.rotX), 0, array, 7, 1);
        Array.Copy(BitConverter.GetBytes(c.rotY), 0, array, 8, 1);
        Array.Copy(BitConverter.GetBytes(c.rotZ), 0, array, 9, 1);

        return array;
    }
    public static object Deserialize(byte[] data)
    {
        Packet result = new Packet();

        result.id = BitConverter.ToInt16(data, 0);
        result.posX = BitConverter.ToInt16(data, 2);
        result.posY = data[4];
        result.posZ = BitConverter.ToInt16(data, 5);
        result.rotX = data[7];
        result.rotY = data[8];
        result.rotZ = data[9];

        return result;
    }
}

struct Snapshot
{
    public Vector3 TargetPosition;
    public Quaternion TargetRotation;
}