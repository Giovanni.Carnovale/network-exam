using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class LittleCubesManager : MonoBehaviourPunCallbacks, IPunObservable
{
    [Header("Parameters")]
    [SerializeField] private int RowsNumber = 30;
    [SerializeField] private int ColsNumber = 30;

    [Header("References")]
    [SerializeField] private GameObject m_LittleCubePrefab;


    private static float timeSinceLastPacket = 0.0f;
    private static float estimatedTimeBetweenPackets = 1.0f / PhotonNetwork.SendRate;

    private List<LittleCube> m_LittleCubeList = new List<LittleCube>();

    public static float TimeSinceLastPacket
    {
        get { return timeSinceLastPacket; }
    }

    public static float EstimatedTimeBetweenPackets
    {
        get { return estimatedTimeBetweenPackets; }
    }

    private void Awake()
    {
        float StartX = -(ColsNumber)/ 4 - 0.2f;
        float StartZ = -(RowsNumber)/ 4 - 0.2f;

        Vector3 SpawnPos = new Vector3(StartX, 0.5f, StartZ);

        for(int i = 0; i<RowsNumber; i++)
        {
            for(int j = 0; j<ColsNumber; j++)
            {
                GameObject go = Instantiate(m_LittleCubePrefab, SpawnPos, Quaternion.identity);
                LittleCube cube = go.GetComponent<LittleCube>();
                cube.Id = (short)((i * ColsNumber) + j);
                m_LittleCubeList.Add(cube);

                SpawnPos.x += 0.7f;
            }
            SpawnPos.x = StartX;
            SpawnPos.z += 0.7f;
        }
    }

    private void Update()
    {
        if (!photonView.IsMine)
            timeSinceLastPacket += Time.deltaTime;
        else
        {
            transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x, -128f, 128f),
                    Mathf.Clamp(transform.position.y, 0, 16f),
                    Mathf.Clamp(transform.position.z, -128f, 128f)
            );
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting && PhotonNetwork.IsMasterClient)
        {
            List<Packet> DataToSend = new List<Packet>();
            foreach(LittleCube cube in m_LittleCubeList)
            {
                if (cube.IsActive())
                {
                    DataToSend.Add(Packet.MakePacket(cube.Id, cube.transform));
                }
            }
            Packet.SendBuffered(stream, ref DataToSend);

            Debug.Log("Sent: " + (float)(DataToSend.Count * Packet.GetSize() * 8f/1000f) + " kbit of little cubes");
        }
        else if (stream.IsReading && !PhotonNetwork.IsMasterClient)
        {
            List<Packet> Packets = Packet.ReceiveBuffered(stream);

            float data = (float)(Packets.Count * Packet.GetSize() * 8f / 1000f);
            Debug.Log("Bandwidth: " + data/TimeSinceLastPacket + "\nReceived: " + data + " kbit of little cubes in " + TimeSinceLastPacket + " s;");


            estimatedTimeBetweenPackets = (1f - Launcher.Instance.Alpha) * estimatedTimeBetweenPackets + (Launcher.Instance.Alpha) * timeSinceLastPacket;
            timeSinceLastPacket = 0.0f;
            foreach(Packet p in Packets)
            {
                Vector3 NetPos = Vector3.zero;
                Quaternion NetRot = Quaternion.identity;
                short id;
                Packet.UnpackPacket(p, out id, out NetPos, out NetRot);
                Assert.IsFalse(id == -1 && id >= 0 && id < m_LittleCubeList.Count, "Received invalid index for small cube");

                m_LittleCubeList[id].FeedSnapshot(NetPos, NetRot);

            }
        }
    }
}
