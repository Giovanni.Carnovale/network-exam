using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FollowCamera : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] private GameObject ObjectToFollow;
    [SerializeField, Range(0.0f, 20.0f)] private float Distance = 5f;
    [SerializeField, Range(-20.0f, 20.0f)] private float Height = 2f;
    [SerializeField, Range(-90f, 90f)] private float OffsetRotation = 20f;

    private void Awake()
    {
        Assert.IsNotNull(ObjectToFollow, "No object to follow in camera");
    }
    void Update()
    {
        this.transform.position = ObjectToFollow.transform.position;

        transform.position += -Vector3.forward * Distance;
        transform.position += Vector3.up * Height;
        transform.LookAt(ObjectToFollow.transform, Vector3.up);
        transform.Rotate(Vector3.right, OffsetRotation, Space.Self);
    }
}
